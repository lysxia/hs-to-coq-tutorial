# hs-to-coq tutorial

[Tutorial on *hs-to-coq*][tutorial], working through a simple example of a Haskell module
verified with *hs-to-coq*.

[tutorial]: https://www.cis.upenn.edu/~plclub/blog/2020-10-09-hs-to-coq/

[[_TOC_]]

## Project layout

- `src/`: Haskell code.
- `src-coq/`: Coq code generated from `src/` using *hs-to-coq*.
- `theories/`: Specifications and proofs.

## Build

The first two sections of the tutorial ("Implementation" and "Translate Haskell
to Coq") only require a Haskell compiler (GHC).

In order to experiment with the proofs of the third section ("Verification"),
you will need to install Coq.

*hs-to-coq* currently requires GHC 8.4 and Coq 8.10.2.

### Install Coq

The easiest way to install Coq is to install it using
[Opam](https://opam.ocaml.org/doc/Install.html).
See also:

- [Install Coq using Opam](https://coq.inria.fr/opam-using.html)
- [Links to download Coq](https://coq.inria.fr/download)

```
# Step 1: Install Opam (see its documentation)

# Step 2: Install Coq
opam install coq.8.10.2
```

Coq programs are edited interactively, so an editor must also be set up for it.
[A list of available interfaces is on Coq's website.](https://coq.inria.fr/user-interfaces.html)

- [VS Code + VsCoq](https://github.com/coq-community/vscoq#readme)
  is likely the most accessible solution nowadays;
- [CoqIDE](https://coq.inria.fr/refman/practical-tools/coqide.html)
  is also an approachable GUI;
- and for people already familiar with the following editors,
  Emacs + [Proof General](https://github.com/ProofGeneral/PG#readme)---and
  optionally [company-coq](https://github.com/cpitclaudel/company-coq#readme);
- or Vim + [Coqtail](https://github.com/whonore/Coqtail#readme).

#### Core default shortcuts

To read or edit Coq files, the main thing to know is how to "step through it".

|                      | VsCoq       | CoqIDE       | PG (Emacs)    | Coqtail (Vim) |
|----------------------|-------------|--------------|---------------|---------------|
| Assert next command  | `alt+down`  | `ctrl+down`  | `C-c C-n`     | `<leader>cj`  |
| Undo last command    | `alt+up`    | `ctrl+up`    | `C-c C-u`     | `<leader>ck`  |
| Go to cursor         | `alt+right` | `ctrl+right` | `C-c C-Enter` | `<leader>cl`  |
| Retract all          | `alt+home`  | `ctrl+home`  | `C-c C-r`     | `<leader>cT`  |

### Read the tutorial

From this point on, you can follow the [tutorial][tutorial] to write and build the
artifacts from scratch.

Below are some slightly more detailed instructions to install *hs-to-coq*
and to build the programs from the tutorial already recorded in this repository.

### Install hs-to-coq

*hs-to-coq* consists of two mostly independent parts:

- the translation tool to convert `.hs` files to `.v`;
- the Haskell standard library translated to a Coq library, plus some theorems.

```
# Download hs-to-coq:
git clone https://github.com/antalsz/hs-to-coq
cd hs-to-coq
```

For reference (and reproducibility), the current version of *hs-to-coq* is commit
`6b17a4acdb21c0280a1313b2c3fccb087da43faa`.

This tutorial repository assumes the *hs-to-coq* repository is a subdirectory
(you can also make it a symbolic link).

```
# Directory structure
hs-to-coq-tutorial/
hs-to-coq-tutorial/hs-to-coq/
```

#### The translation tool

The *hs-to-coq* translation tool is a regular Haskell package,
you can install it using cabal or stack, with GHC 8.4:

```coq
stack install
```

This installs the `hs-to-coq` executable.

#### The standard library

The generated files are included so you don't have to run the *hs-to-coq*
translation, but you still need the Coq-ified *base* library to process the
proofs.

```sh
# Coq dependencies
opam install coq-mathcomp-ssreflect

# Build the Coq-ified standard library
make -C base && make -C base-thy
```

Make the `hs-to-coq` repository a subfolder of this one,
or symlink to it:

```coq
# At the root of this repo
ln -s my/path/to/hs-to-coq hs-to-coq
```

### Build the tutorial artifacts

#### The Haskell implementation

```sh
make hs
```

That calls GHC on [`src/Queue.hs`](./src/Queue.hs):

```
ghc src/Queue.hs
```

#### The Haskell to Coq translation

With *hs-to-coq* installed, this refreshes the `src-coq/` directory:

```sh
make hs2coq
```

That calls hs-to-coq on [`src/Queue.hs`](./src/Queue.hs),
producing [`src-coq/Queue.v`](./src-coq/Queue.v).

```
hs-to-coq -e hs-to-coq/base/edits -e edits --iface-dir hs-to-coq/base src/Queue.hs -o src-coq/
```

The resulting file can be compiled:

```sh
make coq
```

That calls the `coqc` compiler on [`src-coq/Queue.v`](./src-coq/Queue.v):

```
coqc -R hs-to-coq/base "" -Q hs-to-coq/base-thy Proofs -Q src-coq Src -Q theories Proving src-coq/Queue.v
```

#### The Coq specification and proofs

```sh
make
```

That calls the `coqc` compiler on [`theories/QueueSpec.v`](./theories/QueueSpec.v):

```
coqc -R hs-to-coq/base "" -Q hs-to-coq/base-thy Proofs -Q src-coq Src -Q theories Proving theories/QueueSpec.v
```
