.PHONY: proof hs hs2coq

HS_TO_COQ := ./hs-to-coq
HS_TO_COQ_EXE := hs-to-coq
SRC := src
SRC_COQ := src-coq

proof: Makefile.coq
	$(MAKE) -f $<

Makefile.coq: _CoqProject
	coq_makefile -f _CoqProject -o Makefile.coq

hs:
	ghc src/Queue.hs

hs2coq:
	$(HS_TO_COQ_EXE) -e $(HS_TO_COQ)/base/edits -e edits $(SRC)/Queue.hs --iface-dir $(HS_TO_COQ)/base -o $(SRC_COQ)

coq:
	$(MAKE) -f Makefile.coq src-coq/Queue.vo

clean:
	if [ -f Makefile.coq ] ; then $(MAKE) -f Makefile.coq cleanall ; fi
	$(RM) Makefile.coq Makefile.coq.conf
	$(RM) src/*.o src/*.hi src-coq/*.h2ci
